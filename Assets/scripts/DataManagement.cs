﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class DataManagement : MonoBehaviour
{
    public static DataManagement dataManagement;
    public int highScore;
    public int playerScore;

    void Awake()
    {
        if(dataManagement == null)
        {
            DontDestroyOnLoad(gameObject);
            dataManagement = this;
        } else if(dataManagement != this){
            Destroy(gameObject);
        }
    }

    public void SaveData()
    {
        BinaryFormatter BinForm = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/gameInfo.dat");
        GameData data = new GameData();
        data.highScore = highScore;
        data.playerScore = playerScore;
        BinForm.Serialize(file, data);
        file.Close();
    }

    public void LoadData()
    {
        if(File.Exists(Application.persistentDataPath + "/gameInfo.dat"))
        {
            BinaryFormatter BinForm = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/gameInfo.dat", FileMode.Open);
            GameData data = (GameData)BinForm.Deserialize(file);
            file.Close();
            highScore = data.highScore;
            playerScore = data.playerScore;
        }
    }
}

[Serializable]
class GameData
{
    public int highScore;
    public int playerScore;
}