﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class EventSystemChecker : MonoBehaviour
{
	void OnLevelWasLoaded ()
	{
		if(!FindObjectOfType<EventSystem>())
		{
			GameObject obj = new GameObject("EventSystem");

			//And adds the required components
			obj.AddComponent<EventSystem>();
			obj.AddComponent<StandaloneInputModule>().forceModuleActive = true;
			obj.AddComponent<TouchInputModule>();
		}
	}
}
